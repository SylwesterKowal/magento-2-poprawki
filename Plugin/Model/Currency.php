<?php

namespace Kowal\Poprawki\Plugin\Model;

class Currency
{
    public function afterformatTxt(\Magento\Directory\Model\Currency $subject, $result)
    {
        if (strpos($result, '€') !== false) {
            $result = str_replace('€', '', $result) . ' €';
        } elseif (strpos($result, 'zł') !== false) {
            $result = str_replace('zł', '', $result) . ' zł';
        }
        return $result;
    }
}
